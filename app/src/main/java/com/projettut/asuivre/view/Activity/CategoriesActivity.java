package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.projettut.asuivre.view.Fragment.CategoriesFragment;
import com.projettut.asuivre.view.Activity.HomeActivity;
import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;

public class CategoriesActivity extends AppCompatActivity implements CategoriesFragment.OnFragmentInteractionListener {
    private Control control;
    private FragmentManager fragmentManager = null;
    public Fragment fragment = null;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        fragmentManager = getSupportFragmentManager();
        fragment = new CategoriesFragment();
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();

        control = Control.getInstance();
        control.setActivity(this);
        control.selectCategories();

        BottomNavigationView navigation = findViewById(R.id.navigation_category);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                        break;
                    case R.id.navigation_profile:
                        Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                        startActivity(i3);
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}