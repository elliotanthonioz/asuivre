package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.Story;
import com.projettut.asuivre.view.Fragment.PargraphsDisplayFragment;

public class FullStoryDisplayActivity extends AppCompatActivity implements PargraphsDisplayFragment.OnFragmentInteractionListener {

    private Story story;
    private TextView text;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;
    public FragmentManager fragmentManager;
    public Fragment pargraphsDisplayFragment;
    private Control control;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_story_display);

        BottomNavigationView navigation = findViewById(R.id.navigation_full_story_display);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                        break;
                    case R.id.navigation_profile:
                        Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                        startActivity(i3);
                        break;
                    case R.id.navigation_categories:
                        Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);



        String story_id = (String) getIntent().getSerializableExtra("Story");

        fragmentManager = getSupportFragmentManager();
        pargraphsDisplayFragment = new PargraphsDisplayFragment();
        fragmentManager.beginTransaction().replace(R.id.paragraph_display_fragment, pargraphsDisplayFragment).commit();
        Log.d("*****", "onCreate: AFTER FRAGMENT");

        control = Control.getInstance(); /*(Control) getIntent().getSerializableExtra("Control");*/
        control.setActivity(this);
        control.selectParagraphsByStory(Integer.parseInt(story_id));
        Log.d("*****", "onCreate: AFTER SELECT");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
