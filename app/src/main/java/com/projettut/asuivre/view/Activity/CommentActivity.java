package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.model.Comment;
import com.projettut.asuivre.model.Story;
import com.projettut.asuivre.view.Fragment.CommentFragmentDisplay;
import com.projettut.asuivre.view.ListAdapter.CommentListAdapter;

import java.util.ArrayList;
import java.util.List;

public class CommentActivity extends AppCompatActivity implements CommentFragmentDisplay.OnFragmentInteractionListener {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;
    private List<Comment> commentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Control control;
    private FragmentManager fragmentManager = null;
    private Button insertCommentButton;
    private TextInputEditText newCommentInput;
    public Fragment fragment = null;
    public String story_id;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        fragmentManager = getSupportFragmentManager();
        fragment = new CommentFragmentDisplay();
        fragmentManager.beginTransaction().replace(R.id.comments_layout_for_recycler_view, fragment).commit();

        control = Control.getInstance();
        control.setActivity(this);
        story_id = (String) getIntent().getExtras().getSerializable("Story");
        control.selectCommentsByStory(Integer.parseInt(story_id));

        BottomNavigationView navigation = findViewById(R.id.navigation_comment);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                break;
                    case R.id.navigation_profile:
                    Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                    startActivity(i3);
                break;
                case R.id.navigation_categories:
                    Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
            }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        newCommentInput = findViewById(R.id.new_comment_input);
        insertCommentButton = findViewById(R.id.insert_comment_button);

        insertCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newCommentInput.getText().toString().equals(""))
                {
                    String toastMessage = "Comment cannot be empty!";
                    Toast.makeText(control.activity, toastMessage, Toast.LENGTH_SHORT).show();
                    return;
                }
                control.insertComment(0, 1, Integer.parseInt(story_id), newCommentInput.getText().toString(), 0);
            }
        });


        /*control = Control.getInstance();
        control.setActivity(this);
        Log.d("chemin1", "onCreate: ");
        if (getIntent().hasExtra("Comment"))
        {
            Log.d("trasnfert", String.valueOf(getIntent().getExtras().getInt("Story")) );
            control.selectCommentsByStory(getIntent().getExtras().getInt("Story"));
        }
        else
        {
            control.selectStories();
        }*/

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
