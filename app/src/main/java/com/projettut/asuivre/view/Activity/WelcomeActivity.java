package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "MainActivity";
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInClient mGoogleSignInClient;
    private Control control = Control.getInstance();
    private String firstName, lastName, pseudo, email;
    private Date birthDate = new Date(-20, 1, 20);
    private FirebaseAuth mAuth;
    private int RC_SIGN_IN = 7;
    private GoogleSignInAccount acctG;
    private GoogleSignInOptions gso;
    private CallbackManager mCallbackManager;
    private static boolean google = false;
    private static boolean facebook = false;
    private LoginButton loginButton;
    private FirebaseUser firebaseUser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        findViewById(R.id.sign_in_button_google).setOnClickListener(this);
        findViewById(R.id.sign_in_button_facebook).setOnClickListener(this);
        control.setActivity(this);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user;

        Log.d("CURRENT USER", String.valueOf(mAuth.getCurrentUser()));
        if(mAuth.getCurrentUser() != null){
            user = mAuth.getCurrentUser();
            Log.d("***", "onCreate: " + user.getEmail());
            //control.selectUserByEmail(mAuth.getCurrentUser().getEmail());
            goToHome();
        }

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button_google:
                Log.d("google", "google");
                google = true;

                signIn();

            case R.id.sign_in_button_facebook:
                facebook = true;

                FacebookSdk.sdkInitialize(getApplicationContext());
                mCallbackManager = CallbackManager.Factory.create();

                loginButton = findViewById(R.id.sign_in_button_facebook);
                loginButton.setReadPermissions("email", "public_profile");

                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("Main", response.toString());
                                setProfileToView(object);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields","id,first_name,last_name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                        Log.d(TAG, "facebook:onSuccess:" + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "facebook:onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG, "facebook:onError", error);
                    }
                });
                Log.d("logged : ",String.valueOf(isLoggedIn()));
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                    }
                });
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == RC_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    firebaseAuthWithGoogle(account);
                    Log.d("Account", String.valueOf(account));
                    acctG = account;
                    pseudo = acctG.getDisplayName();
                    firstName = acctG.getGivenName();
                    lastName= acctG.getFamilyName();
                    email = acctG.getEmail();

                    Log.d("pseudo", acctG.getDisplayName());
                    control.selectUserByEmail(email);

                } catch (ApiException e) {
                    Log.w(TAG, "Google sign in failed", e);
                    // ...
                }
            }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("ERREUR BIP BIP");
    }

    public static void signOut(){
        if(google)google=false;
        if(facebook)facebook=false;
        FirebaseAuth.getInstance().signOut();
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    public void setProfileToView(JSONObject jsonObject){
        try {
            firstName = jsonObject.getString("first_name");
            lastName = jsonObject.getString("last_name");
            pseudo = firstName+lastName;
            email = jsonObject.getString("email");

            Log.d("firstname", jsonObject.getString("first_name"));
            Log.d("lastname", jsonObject.getString("last_name"));
            if(jsonObject.getString("email") == null) email = "noemail";
            Log.d("email", jsonObject.getString("email"));

            control.selectUserByEmail(email);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goToHome(){
        Intent iH = new Intent(getBaseContext(), HomeActivity.class);
        startActivity(iH);
    }

    public void newUser(){
        control = Control.getInstance();
        Log.d("EMAIL", "email" + email);
        Log.d("EXISTENCE", String.valueOf(control.isUserExists()));

        if(!control.isUserExists()) {
            control.insertUser(0, firstName, lastName, pseudo, birthDate, 0, email);
            /*firebaseUser = mAuth.createUserWithEmailAndPassword(email, email);
            mAuth.getCurrentUser().updateEmail(email);*/
        }
        goToHome();
    }
}