package com.projettut.asuivre.view.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.projettut.asuivre.R;

public class CommentViewHolder extends RecyclerView.ViewHolder {

    public TextView userName, commentText;
    public Button reportButton;
    /*public ImageView userPicture;*/


    public CommentViewHolder(@NonNull View itemView) {
        super(itemView);
        userName = (TextView) itemView.findViewById(R.id.buttonuserprofile);
        commentText = (TextView) itemView.findViewById(R.id.textcomment);
        reportButton = itemView.findViewById(R.id.button_report_comment);
        /*userpicture = (ImageView) itemView.findViewById(R.id.UNCERTAINEIMAGE);*/
    }
}
