package com.projettut.asuivre.view.ListAdapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.projettut.asuivre.view.ViewHolder.CategoryViewHolder;
import com.projettut.asuivre.R;
import com.projettut.asuivre.model.Category;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<com.projettut.asuivre.model.Category> categoryList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public MyViewHolder(TextView v) {
            super(v);
            textView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoryListAdapter(List<Category> categoriesList) {
        categoryList = categoriesList;
    }

    // Create new views (invoked by the layout manager)
    @NotNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_category,parent,false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder categoryViewHolder, int i) {
        Category category = categoryList.get(i);
        categoryViewHolder.categoryTextView.setText(category.getCategory_name());
        Log.d("***", "onBindViewHolder: " + category.getCategory_name());
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
