package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.projettut.asuivre.R;
import com.projettut.asuivre.controleur.Control;
import com.projettut.asuivre.view.Fragment.HomeFragment;

public class HomeActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener {

    private Control control;
    private FragmentManager fragmentManager = null;
    public Fragment fragment = null;
    public String text;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        fragmentManager = getSupportFragmentManager();
        fragment = new HomeFragment();
        fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();

        BottomNavigationView navigation = findViewById(R.id.navigation_h);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_categories:
                        Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
                    case R.id.navigation_profile:
                        startActivity(new Intent(getBaseContext(), UserActivity.class));
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FloatingActionButton createStoryButton = findViewById(R.id.add_story);
        createStoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, CreateStoryActivity.class));
            }
        });

        control = Control.getInstance();
        control.setActivity(this);
        control.shareActivity = this;
        if (getIntent().hasExtra("Category"))
        {
            control.selectStoriesByCategory(getIntent().getExtras().getInt("Category"));
        }
        else
        {
            control.selectStories();
        }
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        Log.d("***", "onCreate: " + mAuth.getCurrentUser().getProviderData().get(1).getEmail());
        control.email = mAuth.getCurrentUser().getProviderData().get(1).getEmail();
        control.selectUserByEmail(control.email);
        //control.selectLikes();
        //control.selectParagraphsByStory(1);
        //control.selectCommentsByStory(1);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }








    /*public void getStory()
    {
        TextView storyTextView = findViewById(R.id.story);
        storyTextView.setText(control.getStoriesList().get(0).getStory_name());
    }*/

}
