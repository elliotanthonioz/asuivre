package com.projettut.asuivre.view.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.facebook.login.LoginManager;
import com.projettut.asuivre.R;

public class UserActivity extends AppCompatActivity {
    Button button;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        button = findViewById(R.id.deconnexion);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                WelcomeActivity.signOut();
                LoginManager.getInstance().logOut();
                Intent i1 = new Intent(getBaseContext(), WelcomeActivity.class);
                startActivity(i1);
            }
        });

        BottomNavigationView navigation = findViewById(R.id.navigation_user);
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i1 = new Intent(getBaseContext(), HomeActivity.class);
                        startActivity(i1);
                        break;
                    case R.id.navigation_profile:
                        Intent i3 = new Intent(getBaseContext(), UserActivity.class);
                        startActivity(i3);
                        break;
                    case R.id.navigation_categories:
                        Intent i2 = new Intent(getBaseContext(), CategoriesActivity.class);
                        startActivity(i2);
                        break;
                }
                return true;
            }
        };
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
}
