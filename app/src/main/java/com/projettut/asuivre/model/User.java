package com.projettut.asuivre.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    int id;
    String user_firstname;
    String user_lastname;
    String user_pseudo;
    Date user_birthdate;
    String email;
    int report;


    public User(int id, String user_firstname, String user_lastname, String user_pseudo, Date user_birthdate, int report, String email) {
        this.id = id;
        this.user_firstname = user_firstname;
        this.user_lastname = user_lastname;
        this.user_pseudo = user_pseudo;
        this.user_birthdate = user_birthdate;
        this.report = report;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_firstname() {
        return user_firstname;
    }

    public void setUser_firstname(String user_firstname) {
        this.user_firstname = user_firstname;
    }

    public String getUser_lastname() {
        return user_lastname;
    }

    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    public String getUser_pseudo() {
        return user_pseudo;
    }

    public void setUser_pseudo(String user_pseudo) {
        this.user_pseudo = user_pseudo;
    }

    public Date getUser_birthdate() {
        return user_birthdate;
    }

    public void setUser_birthdate(Date user_birthdate) {
        this.user_birthdate = user_birthdate;
    }

    public int getReport() {
        return report;
    }

    public void setReport(int report) {
        this.report = report;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public JSONArray convertToJSONArray(){
        List<Serializable> list = new ArrayList<>();
        list.add(this.id);
        list.add(this.user_firstname);
        list.add(this.user_lastname);
        list.add(this.user_lastname);
        list.add(this.user_birthdate);
        list.add(this.report);
        list.add(this.email);

        return new JSONArray(list);
    }
}
