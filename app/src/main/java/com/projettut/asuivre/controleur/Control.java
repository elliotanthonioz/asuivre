package com.projettut.asuivre.controleur;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import com.projettut.asuivre.view.Activity.HomeActivity;
import com.projettut.asuivre.model.AccessDistant;
import com.projettut.asuivre.model.Category;
import com.projettut.asuivre.model.Comment;
import com.projettut.asuivre.model.Like;
import com.projettut.asuivre.model.Paragraph;
import com.projettut.asuivre.model.Story;
import com.projettut.asuivre.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Control implements Serializable {

    private static Control control_instance = null;

    // Variables story
    private Story story;
    private ArrayList<Story> storiesList;
    // Variables category
    private Category category;
    private ArrayList<Category> categoriesList;
    // Variables paragraph
    private Paragraph paragraph;
    private ArrayList<Paragraph> paragraphsList;
    // Variables CommentFragmentDisplay
    private Comment comment;
    private ArrayList<Comment> commentsList;
    // Variables User
    private boolean userExists;
    private User user;
    private ArrayList<User> usersList;
    public String email;
    // Variables Like
    private Like like;
    private ArrayList<Like> likesList;
    public int likeNum;

    public AccessDistant accessDistant;
    public Activity activity;
    public Activity shareActivity;
    public TextView textViewLike;

    /*public Control(){ super(); }*/
    private Control() { super(); }

    public static Control getInstance()
    {
        if  (control_instance == null)
        {
            control_instance = new Control();
        }
        return control_instance;
    }


    // STORY
    public void selectStoriesByCategory(int category_id){
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectStoriesByCategory%"+category_id, new JSONArray());
    }
    public void selectStories(){
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectStory", new JSONArray());
    }
    public void insertStory(int id, int category_id, int user_id, String story_name ){
        this.story = new Story(id,category_id,user_id,story_name);
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("insertStory", this.story.convertToJSONArray());
    }

    public void setStory(Story story)
    {
        this.story = story;
    }

    public ArrayList<Story> getStoriesList() {
        while (storiesList == null){}
        return storiesList;
    }

    public void setStoriesList(ArrayList<Story> storiesList) {
        this.storiesList = storiesList;
    }

    public Story getStory()
    {
        return this.story;
    }

    public void recupStory(String message){
        try {
            JSONArray responseJson = new JSONArray(message);
            ArrayList<Story> storiesList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                JSONObject currentStory = responseJson.getJSONObject(i);
                int id = currentStory.getInt("id");
                int category_id = currentStory.getInt("category_id");
                int user_id = currentStory.getInt("user_id");
                String story_name = currentStory.getString("story_name");
                String pararaph_text = currentStory.getString("paragraph_text");
                Story story = new Story(id,category_id,user_id,story_name);
                story.setFirstParagraph(pararaph_text);
                storiesList.add(story);
            }
            setStoriesList(storiesList);
            Log.d("Stories added", String.valueOf(storiesList.size()));
        } catch (JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());

        }
    }

    public void recupLikesNum(String message)
    {
        try {
            JSONArray responseJson = new JSONArray(message);
            for (int i = 0; i < responseJson.length(); i++)
            {
                JSONObject like = responseJson.getJSONObject(i);
                likeNum = like.getInt("like_count");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }










    // CATEGORY
    public void selectCategories(){
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectCategory", new JSONArray());
    }
    // Pas de insert, pas besoin d'ajouter une categorie pour un utilisateur

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public ArrayList<Category> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(ArrayList<Category> categoriesList) {
        this.categoriesList = categoriesList;
    }
    public void recupCategory(String message){
        try {
            JSONArray responseJson = new JSONArray(message);
            ArrayList<Category> categoriesList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                JSONObject currentCategory = responseJson.getJSONObject(i);
                int id = currentCategory.getInt("id");
                String category_name = currentCategory.getString("category_name");
                int required_majority = currentCategory.getInt("required_majority");
                Category category = new Category(id,category_name,required_majority);
                categoriesList.add(category);
            }
            setCategoriesList(categoriesList);
            Log.d("Categories added", String.valueOf(categoriesList.size()));
        } catch (JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());

        }
    }












    // PARAGRAPHS
    public void insertParagraph(int id, int user_id, int story_id, String paragraph_text, int report ){
        comment = new Comment(id, user_id,story_id, paragraph_text, report);
        Log.d("***", "insertParagraph: " + story_id + "   " + paragraph_text);
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("insertParagraph", comment.convertToJSONArray());
    }

    public void selectParagraphsByStory(int id, TextView textViewHistoire) {
        Log.d("functionSelectPara", "functionSelectPara ");
        accessDistant = new AccessDistant(this, activity, textViewHistoire);
        accessDistant.send("selectParagraphsByStory%"+id, new JSONArray());
    }

    public void selectParagraphsByStory(int story_id){
        Log.d("functionSelectPara", "functionSelectPara ");
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectParagraphsByStory%"+story_id, new JSONArray());
    }
    public Paragraph getParagraph() {
        return this.paragraph;
    }

    public void setParagraph(Paragraph paragraph) {
        this.paragraph = paragraph;
    }

    public ArrayList<Paragraph> getParagraphsList() {
        return this.paragraphsList;
    }

    public void setParagraphsList(ArrayList<Paragraph> paragraphsList) {
        this.paragraphsList = paragraphsList;
    }

    public void reportParagraph(int paragraph_id) {
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("reportParagraph%" + String.valueOf(paragraph_id), new JSONArray());
    }

    public void recupParagraph(String message){
        try {
            JSONArray responseJson = new JSONArray(message);
            ArrayList<Paragraph> paragraphsList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                JSONObject currentParagraph = responseJson.getJSONObject(i);
                int id = currentParagraph.getInt("id");
                int user_id = currentParagraph.getInt("user_id");
                int story_id = currentParagraph.getInt("story_id");
                String paragraph_text = currentParagraph.getString("paragraph_text");
                int reportings = currentParagraph.getInt("reportings");
                Paragraph paragraph = new Paragraph(id,user_id,story_id,paragraph_text,reportings);
                paragraphsList.add(paragraph);
            }
            setParagraphsList(paragraphsList);
            Log.d("Paragraphs added", String.valueOf(paragraphsList.size()));
        } catch (JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());

        }
    }











    // COMMENT
    public void insertComment(int id, int user_id, int story_id, String commantary_text, int report ){
        comment = new Comment(id, user_id,story_id, commantary_text, report);
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("insertComment", comment.convertToJSONArray());
    }
    public void selectCommentsByStory(int story_id){
        Log.d("funcSelectComment", "funcSelectComment");
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectCommentsByStory%"+story_id, new JSONArray());
    }


    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public ArrayList<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(ArrayList<Comment> commentsList) {
        this.commentsList = commentsList;
    }

    public void reportComment(int comment_id)
    {
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("reportComment%" + String.valueOf(comment_id), new JSONArray());
    }

    public void recupComment(String message){
        try{
            JSONArray responseJson = new JSONArray(message);
            ArrayList<Comment> commentArrayList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                //Log.d("***", "recupComment: INFOR");
                JSONObject currentParagraph = responseJson.getJSONObject(i);
                int id = currentParagraph.getInt("id");
                int user_id = currentParagraph.getInt("user_id");
                int story_id = currentParagraph.getInt("story_id");
                String paragraph_text = currentParagraph.getString("commentary_text");
                int reportings = currentParagraph.getInt("report");
                Comment comment = new Comment(id,user_id,story_id,paragraph_text,reportings);
                commentArrayList.add(comment);
            }
            setCommentsList(commentArrayList);
            Log.d("Comments added", String.valueOf(commentArrayList.size()));
        } catch (JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());

        }
    }







    // USER
    public void selectUserByEmail(String email){
        Log.d("funcSelectUsersByEmail", "funcSelectUsersByEmail");
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectUserWithEmail%"+email, new JSONArray());
    }
    public void insertUser(int id, String user_firstname, String user_lastname, String user_pseudo, Date user_birthdate, int report, String email ){
        // TODO peut etre convertir la date en string flemme de tester
        this.user = new User(id, user_firstname,user_lastname, user_pseudo, null, report, email);
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("insertUser", user.convertToJSONArray());
    }
    public void selectUsers(){
        Log.d("funcSelectUsers", "funcSelectUsers");
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectUsers", new JSONArray());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(ArrayList<User> usersList) {
        this.usersList = usersList;
    }

    public void recupUsers(String message){
        try {
            JSONArray responseJson = new JSONArray(message);
            ArrayList<User> usersList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                JSONObject currentUser = responseJson.getJSONObject(i);
                int id = currentUser.getInt("id");
                String user_firstname = currentUser.getString("user_firstname");
                String user_lastname = currentUser.getString("user_lastname");
                String user_pseudo = currentUser.getString("user_pseudo");
                String user_birthdate = currentUser.getString("user_birthday");
                Date date = new SimpleDateFormat("dd/mm/yyyy").parse(user_birthdate);
                String email = currentUser.getString("email");
                int report = currentUser.getInt("report");
                User user = new User(id, user_firstname, user_lastname, user_pseudo, date, report, email);
                usersList.add(user);
            }
            setUsersList(usersList);
            Log.d("Stories added", String.valueOf(usersList.size()));
        } catch (
                JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());

        } catch (
                ParseException e) {
            Log.d("Error conv date", "selectUsers->Date format");
        }
    }

    public boolean isUserExists() {
        return userExists;
    }

    public void setUserExists(boolean userExists) {
        this.userExists = userExists;
    }



    // LIKE
    public void insertLike(int id, int user_id, int story_id){
        this.like = new Like(id, story_id,user_id);
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("insertLike", like.convertToJSONArray());
    }
    public void selectLikes(){
        Log.d("funcSelectLikes", "funcSelectLikes");
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectLikes", new JSONArray());
    }

    public Like getLike() {
        return like;
    }

    public void setLike(Like like) {
        this.like = like;
    }

    public ArrayList<Like> getLikesList() {
        return likesList;
    }

    public void setLikesList(ArrayList<Like> likesList) {
        this.likesList = likesList;
    }

    public void selectLikesByStory(int story_id)
    {
        accessDistant = new AccessDistant(this, activity);
        accessDistant.send("selectLikesByStory%" + story_id, new JSONArray());
    }

    public void recupLike(String message){
        try{
            JSONArray responseJson = new JSONArray(message);
            ArrayList<Like> likesList = new ArrayList<>();
            for (int i = 0; i < responseJson.length(); i++) {
                JSONObject currentLike = responseJson.getJSONObject(i);
                int id = currentLike.getInt("id");
                int user_id = currentLike.getInt("user_id");
                int story_id = currentLike.getInt("story_id");
                Like like = new Like(id,story_id,user_id);
                likesList.add(like);
            }
            setLikesList(likesList);
            Log.d("Likes added", String.valueOf(likesList.size()));
        } catch (JSONException e) {
            Log.d("Error", "Conversion to JSON impossible" + e.toString());
        }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setTextViewLike(TextView textViewLike) {
        this.textViewLike = textViewLike;
    }
}